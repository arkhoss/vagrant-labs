#!/bin/bash
# Usage: docker-install-script.sh
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

echo "Remove OLd"
sudo apt-get remove docker docker-engine docker.io containerd runc

echo "Updating the system..."
sudo apt-get update

sudo apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

echo "Updating the system..."
sudo apt-get update

echo "Installing docker-ce"
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

echo "Updating the system..."
sudo apt-get update

echo "Setting up the docker service and user"
sudo systemctl start docker

sudo groupadd docker

sudo usermod -aG docker $USER

sudo systemctl enable docker

echo "Running test container"
sudo docker run hello-world

sudo apt-get install -y net-tools

echo "Completed"
